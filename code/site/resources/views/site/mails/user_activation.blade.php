<h2>Hi {{ $user->name }},</h2>
<p>
Thank you for your interest in our application.
</p>
<p>
    We would like to confirm your email. Please visit the following link
    <a href='{{ route('site-activate-account', ['token' => $token]) }}'>Activate account</a>
</p>