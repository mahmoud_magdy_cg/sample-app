@if (count($errors) > 0)
 <div class="ui error form">
    <div class="ui message">
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
 </div>
<div class="ui hidden divider"></div>
@endif