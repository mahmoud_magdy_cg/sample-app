@extends('site.layouts.center')
@section('center-content')
    <h2>Resend activation succeeded!</h2>
    <p> An activation mail has been sent to your email</p>
    <a href='{{ route('site-login') }}'>Login</a>
@endsection
