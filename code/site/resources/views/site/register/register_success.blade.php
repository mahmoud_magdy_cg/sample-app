@extends('site.layouts.center')
@section('center-content')
    <h2>Sign up succeeded!</h2>
    <p> Thank you for your registration. an activation email has been sent to your account.</p>
    <a href='{{ route('site-login') }}'>Login</a>
@endsection
