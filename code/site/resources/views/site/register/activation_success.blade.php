@extends('site.layouts.center')
@section('center-content')
    <h2>Activation succeeded!</h2>
    <p> Your email has been verified</p>
    <a href='{{ route('site-login') }}'>Login</a>
@endsection
