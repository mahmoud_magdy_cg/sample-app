@extends('site.layouts.center')
@section('center-content')
    <h2>Sign up</h2>
    <div class='ui divider'></div>
    <form class='ui form' action=' {{ route('site-register') }}' method='post'>
        @include('site.common.form_messages')
        {{csrf_field()}}
        <div class="field">
            <label>Full Name</label>
            <input type="text" name="name" placeholder="" value='{{ old('name') }}'/>
        </div>
        <div class="field">
            <label>Email</label>
            <input type="text" name="email" placeholder="" value='{{ old('email') }}'/>
        </div>
        <div class="field">
            <label>Password</label>
            <input type="password" name="password"/>
        </div>
        <div class="field">
            <label>Retype Password</label>
            <input type="password" name="password_confirmation"/>
        </div>
        <div class="field">
            <label>Gender</label>
            <div class='ui selection dropdown'>
                <input type='hidden' name='gender'/>
                <div class='default text'>select gender</div>
                <i class='dropdown icon'></i>
                <div class='menu'>
                    <div class='item' data-value='male'>Male</div>
                    <div class='item' data-value='female'>Female</div>
                </div>
            </div>
        </div>
        <input type="submit" class="ui green button" value="Sign up"/>
    </form>
    <div class='ui divider'></div>
    <p> 
        <strong> Already have an account! </strong> 
        <a href='{{ route('site-login') }}'>Login</a> 
    </p>
@endsection
