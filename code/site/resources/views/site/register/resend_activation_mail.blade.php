@extends('site.layouts.center')
@section('center-content')
    <h2>Resend acctivation email</h2>
    <div class='ui divider'></div>
    <form class='ui form' action=' {{ route('site-resend-activation-mail') }}' method='post'>
        @include('site.common.form_messages')
        {{csrf_field()}}
        <div class="field">
            <label>Email</label>
            <input type="text" name="email" placeholder="" value='{{ old('email') }}'/>
        </div>
        <input type="submit" class="ui green button" value="send"/>
    </form>
    <div class='ui divider'></div>
    <p> 
        <strong> Already have an account! </strong> 
        <a href='{{ route('site-login') }}'>Login</a> 
    </p>
    <p> 
        <strong> Don't have an account! </strong> 
        <a href='{{ route('site-register') }}'>Sign up</a> 
    </p>

@endsection
