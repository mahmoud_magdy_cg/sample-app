@extends('site.layouts.center')
@section('title', $title)

@section('center-content')
    <h2>Edit Account</h2>
    <form class='ui form' action=' {{ route('site-edit-account') }}' method='post'>
        @include('site.common.form_messages')
        {{csrf_field()}}
        <div class="field">
            <label>Full Name</label>
            <input type="text" name="name" placeholder="" value='{{ old('name', $user->name) }}'/>
        </div>
        <div class="field">
            <label>Gender</label>
            <div class='ui selection dropdown'>
                <input type='hidden' name='gender'/>
                <div class='default text'>select gender</div>
                <i class='dropdown icon'></i>
                <div class='menu'>
                    <div class='item' data-value='male'>Male</div>
                    <div class='item' data-value='female'>Female</div>
                </div>
            </div>
        </div>

        <div class="field">
            <label>Password</label>
            <input type="password" name="password" placeholder='unchanged'/>
        </div>
        <div class="field">
            <label>Retype Password</label>
            <input type="password" name="password_confirmation" placeholder='unchanged'/>
        </div>
        <input type="submit" class="ui green button" value="Save"/>
    </form>
@endsection
