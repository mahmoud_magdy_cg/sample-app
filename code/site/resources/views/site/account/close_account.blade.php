@extends('site.layouts.center')
@section('title', $title)

@section('center-content')
    <h2>Close Account</h2>
    <form class='ui form' action=' {{ route('site-close-account') }}' method='post'>
        @include('site.common.form_messages')
        {{csrf_field()}}
        <div class="field">
            <label>Close Reason</label>
            <textarea name="close_reason">{{ old('close_reason') }}</textarea>
        </div>
        <input type="submit" class="ui green button" value="close"/>
    </form>
@endsection
