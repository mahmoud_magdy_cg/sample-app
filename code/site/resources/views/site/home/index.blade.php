@extends('site.layouts.center')
@section('title', $title)

@section('center-content')
<p>Hi {{ $user->name}}</p>
<ul>
    <li>
        <a href='{{route('site-edit-account')}}'>Edit account</a>
    </li>
    <li>
        <a href='{{route('site-close-account')}}'>close account</a>
    </li>
    <li>
        <a href='{{route('site-logout')}}'>logout</a>
    </li>
</ul>
@endsection
