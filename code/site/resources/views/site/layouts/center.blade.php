@extends('site.layouts.base')
@section('content')
<div class='ui container'>
    <div class='ui hidden divider'></div>
    <div class='ui hidden divider'></div>
    <div class='ui hidden divider'></div>
    <div class='ui centered grid'>
        <div class='ui eight wide column'>
            @yield('center-content')
        </div>
    </div>
</div>

@endsection
