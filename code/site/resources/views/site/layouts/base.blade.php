<!DOCTYPE HTML>
<html>
    <head>
        <title>@yield('title')</title>
        <link rel='stylesheet' href="{{ hashed_asset('assets/vendor.css') }}"/>
        <link rel='stylesheet' href="{{ hashed_asset('assets/main.css') }}"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
         @yield('content')
        <script src='{{ hashed_asset('assets/bootstrap.js') }}'></script>
        <script src='{{ hashed_asset('assets/vendor.js') }}'></script>
        <script src='{{ hashed_asset('assets/main.js') }}'></script>
    </body>
</html>