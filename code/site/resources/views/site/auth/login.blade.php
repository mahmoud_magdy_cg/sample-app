@extends('site.layouts.center')
@section('title', $title)
@section('center-content')
    <h2>Login</h2>
    <div class='ui divider'></div>
    <form class='ui form' action=' {{ route('site-login') }}' method='post'>
        @include('site.common.form_messages')
        {{csrf_field()}}
        <div class="field">
            <label>Email</label>
            <input type="text" name="email" placeholder=""/>
        </div>
        <div class="field">
            <label>Password</label>
            <input type="password" name="password"/>
        </div>
        <input type="submit" class="ui green button" value="Login"/>
    </form>
    <div class='ui divider'></div>
    <p> 
        <strong> Don't have an account! </strong> 
        <a href='{{ route('site-register') }}'>Sign up</a> 
    </p>

@endsection
