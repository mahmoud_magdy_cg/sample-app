<?php if (!function_exists('hashed_asset')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function hashed_asset($path)
    {
        $mapPath = __DIR__.'/../../public/assets/assets-map.json';
        
        if(file_exists($mapPath)){
            $json = json_decode(file_get_contents($mapPath), TRUE);
            
            if($json && isset($json[$path]))
            {
                return asset($json[$path]);
            }    
        }
        return asset($path);
    }
}?>