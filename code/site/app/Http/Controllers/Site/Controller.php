<?php

namespace App\Http\Controllers\Site;
use Auth;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function getCurrentUser()
    {
        return Auth::user();
    }

    function getTitle($baseTitle = '')
    {
        $mainTitle = 'Sample App';
        return $baseTitle ? $baseTitle .' | '.$mainTitle:$mainTitle;
    }
}
