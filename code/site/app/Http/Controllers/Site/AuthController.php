<?php
namespace App\Http\Controllers\Site;

use App\Repositories\UsersRepository;
use Illuminate\Http\Request;
use Auth;
/**
 * 
 */
class AuthController extends Controller {
    
    protected $usersRepository;

    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    public function login(Request $request){
        if($request->isMethod('post')){
            $email = $request->input('email');
            $password = $request->input('password');
            $credentials = ['email' => $email, 'password' => $password, 'is_closed' => 0, 'is_active'=>1];

            if (Auth::guard()->attempt($credentials)){
                return redirect()->intended(route('site-home'));
            }
        }
        $data['title'] = 'Login';
        return view('site.auth.login', $data);
    }

    function logout(){
        Auth::logout();
        return redirect(route('site-login'));
    }
}