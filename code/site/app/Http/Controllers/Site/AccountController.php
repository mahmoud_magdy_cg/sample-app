<?php
namespace App\Http\Controllers\Site;


use App\Repositories\UsersRepository;
use Illuminate\Http\Request;

class AccountController extends Controller {
   
    protected $usersRepository;

    public function __construct(UsersRepository $usersRepository)
    {
        $this->middleware('auth.site',['except']);
        $this->usersRepository = $usersRepository;
    }

    public function edit(Request $request) 
    {
        $user = $this->getCurrentUser();
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required|max:255',
                'password'=> 'confirmed',      
            ]);

            $userData = [
                'name'=>$request->input('name')
                ];

            if($request->input('password'))
                $userData['password'] = bcrypt($request->input('password'));
           
            $this->usersRepository->update($user->id, $userData);
            return redirect(route('site-home'));
        }

        $data = [];
        $data['title'] = $this->getTitle('Edit Account');
        $data['user'] = $user;
        return view('site.account.edit_account',$data);
    }

    public function close(Request $request) 
    {
        $user = $this->getCurrentUser();
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'close_reason' => 'required'       
            ]);

            $userData = [
                'close_reason'=>$request->input('close_reason'),
                'is_closed'=>1
                ];
            $this->usersRepository->update($user->id, $userData);
            return redirect(route('site-home'));
        }

        $data = [];
        $data['title'] = $this->getTitle('Close Account');
        $data['user'] = $user;
        return view('site.account.close_account',$data);        
    }

}