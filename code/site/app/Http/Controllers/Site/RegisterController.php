<?php
namespace App\Http\Controllers\Site;

use App\Mail\UserActivation;
use App\Repositories\UsersRepository;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

use App\Services\UserTokensService;

use Lcobucci\JWT\Parser;

/**
 * 
 */
class RegisterController extends Controller {
    
    protected $usersRepository;

    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

     function register(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users',
                'password'=> 'required|confirmed',        
            ]);

            $userData = [
                'name'=>$request->input('name'),
                'email'=>$request->input('email'),
                'password' => bcrypt($request->input('password'))
                ];
            $user = $this->usersRepository->create($userData);
            Mail::to($user)->send(new UserActivation($user));
            return redirect(route('site-register-success'));
        }

        return view('site.register.register');
    }

    function registerSucceess()
    {
        return view('site.register.register_success');
    }

    function resendActivationSucceess()
    {
        return view('site.register.resend_activation_success');
    }

    public function resendActivationMail(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'email' => 'required|email'       
            ]);
            $email = $request->input('email');
            $user = $this->usersRepository->getByEmail($email);
            if(!$user)
                return back()
                        ->withErrors(['this email is not in our database'])
                        ->withInput();
            Mail::to($user)->send(new UserActivation($user));
            return redirect(route('site-send-activation-success'));
        }

        return view('site.register.resend_activation_mail');
    }

    public function activate(Request $request, UserTokensService $userTokensService)
    {
        $token = $request->input('token');
        if($token) {
            $token =  (new Parser())->parse((string) $token);
            $verified = $userTokensService->verifyToken($token);
            $user = $userTokensService->getUserFromToken($token);
            if(!$user->is_closed){
                $this->usersRepository->update($user->id, ['is_active' => 1]);
                return view('site.register.activation_success');
            }else{
                die("you must provide a valid token");        
            }
        }
        die("you must provide a valid token");
    }
}