<?php
namespace App\Http\Controllers\Site;

class HomeController extends Controller {
    
    public function __construct(){
        $this->middleware('auth.site');
    }

    public function index() 
    {
        $data = [];
        $data['title'] = $this->getTitle('Homepage');
        $data['user'] = $this->getCurrentUser();
        return view('site.home.index',$data);
    }
}