<?php
/**
 * @SWG\Info(title="Sample App API", version="0.1")
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\UsersRepository;


use Validator;
use App\Transformers\UserTransformer;

/**
 * Users Resource API
 */
class UsersController extends Controller{
    
    protected $usersRepository;

    function __construct(   
        UsersRepository $usersRepository
    ){  
        parent::__construct();
        $this->usersRepository = $usersRepository;
		$this->middleware('auth.api');
    }
    
    public function get(Request $request)
    { 
       $users = $this->usersRepository->getPage([],10);
       return $this->generateCollectionPageResponse($users, new UserTransformer);
    }
    
    /**
     * @SWG\Post(path="/users",
     *   tags={"user"},
     *   summary="Create user",
     *   operationId="createUser",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Created user object",
     *     required=true,
     *   ),
     *   @SWG\Response(response="default", description="successful operation")
     * )
     */
    public function create(Request $request)
    {
        $data = $request->json()->all();
        
         $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        if($validator->fails())
            return $this->generateError('invalid input', $validator->errors());
        
        $userData = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ];
        
        if(isset($data['is_active']))
            $userData['is_active'] = $data['is_active'];
        
        $user = $this->usersRepository->create($userData);
        return $this->generateItemResponse($user, new UserTransformer, 201);
    }
    
    public function update($userId, Request $request)
    {
        $data = $request->json()->all();
        
         $validator = Validator::make($data, [
            'name' => 'required|max:255'
        ]);

        if($validator->fails())
            return $this->generateError('invalid input', $validator->errors());
        
        $userData = [
            'name' => $data['name']
        ];

        if(isset($data['is_active']))
            $userData['is_active'] = $data['is_active'];

        if(isset($data['password']))
            $userData['password'] = bcrypt($data['password']);

        $this->usersRepository->update($userId, $userData);
        $user = $this->usersRepository->getById($userId);
        return $this->generateItemResponse($user, new UserTransformer());

    }
    
    public function delete($userId)
    {
        $result = $this->usersRepository->delete($userId);
        if(!$result)
            return $this->generateError("user id doesn't exist");
        return $userId;
    }
    
}