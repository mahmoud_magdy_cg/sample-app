<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\ArraySerializer;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function __construct(){
        $this->middleware('cors');
        $this->fractal = resolve('League\Fractal\Manager');
    }

    function generateError($message, $errors = []){
        return response()->json(['message'=>$message, 'errors'=>$errors],400);
    }

    function getCurrentUser($request){
		$userTokensService = resolve('App\Services\UserTokensService');
		return $userTokensService->getUserFromToken($request->input('token'));
	}

    function generateItemResponse($data, $transformer, $responseCode = 200){
        $resource = new Item($data, $transformer);
        $this->fractal->setSerializer(new ArraySerializer());
        return response()->json($this->fractal->createData($resource)->toArray(), $responseCode);
    }

    function generateCollectionResponse($data, $transformer, $responseCode = 200){
        $resource = new Collection($data, $transformer);
        return response()->json($this->fractal->createData($resource)->toArray(), $responseCode);
    }

    function generateCollectionPageResponse($paginator, $transformer, $responseCode = 200){
        $resource = new Collection($paginator, $transformer);
        $queryParams = array_diff_key($_GET, array_flip(['page']));
        $paginator->appends($queryParams);

        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return response()->json($this->fractal->createData($resource)->toArray(), $responseCode);
    }
}
