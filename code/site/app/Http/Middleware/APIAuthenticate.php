<?php

namespace App\Http\Middleware;

use Closure;


class APIAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
		$token = $request->input('token');
		$verified = FALSE;
		if($token == env('ACCESS_TOKEN')){
            $verified = true;
		}

		if(!$verified)
			return response()->json(['error' => 'token is not verified'],400);
        return $next($request);
    }
}
