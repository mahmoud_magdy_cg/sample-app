<?php
namespace App\Repositories;

class UsersRepository extends \EloquentRepository 
{
    function model()
    {
        return 'App\Models\User';
    }

    function getByEmail($email)
    {
        return $this->model->where('email',$email)->first();
    }

    function generateSecretKey($user){
        $user->secret_key = uniqid();
        $user->save();
        return $user->secret_key;
    }
}