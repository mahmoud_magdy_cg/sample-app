<?php namespace App\Library\Repository;

use App\Library\Repository\Contracts\RepositoryInterface;

abstract class EloquentRepository implements RepositoryInterface{
    private $app;
    protected $model;
    protected $withArray = [];
    protected $idFieldName;
    
    function __construct(){
        $this->makeModel();
    }
    
    abstract function model();

    public function resetModel(){
        $this->makeModel();
    }
    
    public function makeModel(){
        $modelName = $this->model();
        $model = new $modelName;//$this->app->make($this->model());
        /*if(!$model instanceof Model)
            throw new Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        */
        return $this->model = $model;
    }
    
    public function getById($id, $options=[]){
        $this->applyOptions($options);
        $result = $this->model->find($id);
        $this->resetModel();
        return $result;
    }
    
    public function with($withArray = []){
        $this->model = $this->model->with($withArray);
        return $this;
    }
    
    public function withCount($withCountArray = []){
        $this->model = $this->model->withCount($withCountArray);
        return $this;
    }
    
    public function applyOptions($options = []){
        return $this;
    }
    
    public function getAll($options = []){
        $this->applyOptions($options);
       
        $results = $this->model->get();
        $this->resetModel();
        return $results;
    }
    
    public function getPage($options = [] ,$perPage = 15){
        $this->applyOptions($options);
        $results = $this->model->paginate($perPage);
        $this->resetModel();
        return $results;
    }
    public function getCount($options = []){
        $this->applyOptions($options);
        $results = $this->model->count();
        $this->resetModel();
        return $results;
    }   
     
    public function create(array $data){
        return $this->model->create($data);
    }

    public function firstOrCreate(array $data){
        return $this->model->firstOrCreate($data);
    }
    
    public function update($id, array $data){
        return $this->model->find($id)->update($data);
    }
    
    public function delete($id){
        return $this->model->destroy($id);
    }
    
    public function addChildren($id, $key, $childrenIds, $childrenData = []){
        $item = $this->model->find($id);
        for($i=0; $i<count($childrenIds); $i++){
            $item->$key()->attach($childrenIds[$i]);
        }
    }
    
    public function replaceChildren($id, $key, $childrenIds, $childrenData = []){
        $item = $this->model->find($id);
        $item->$key()->detach();
        
        for($i=0; $i<count($childrenIds); $i++){
            $item->$key()->attach($childrenIds[$i]);
        }
    }
}

