<?php namespace App\Library\Repository\Contracts;

interface RepositoryInterface {

    public function getAll();

    public function getPage($perPage = 15);

    public function create(array $data);

    public function update($id, array $data);

    public function delete($id);

}