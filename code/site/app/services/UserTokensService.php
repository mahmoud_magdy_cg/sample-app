<?php
namespace App\Services;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Builder;

class UserTokensService{
   public function createToken($user, $timeToLive = 60*60*24){
		if(!$user->secret_key){
			$secretKey = resolve('App\Repositories\UsersRepository')->generateSecretKey($user);
		}else
			$secretKey = $user->secret_key;
		$signer = new Sha256();
		$token = (new Builder())->setIssuer('sample_app') // Configures the issuer (iss claim)
                        ->setAudience('sample _app user') // Configures the audience (aud claim)
                        ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
                        ->setNotBefore(time()) // Configures the time that the token can be used (nbf claim)
                        ->setExpiration(time() + $timeToLive) // Configures the expiration time of the token (nbf claim)
                        ->set('userId',  $user->id) // Configures a new claim, called "uid"
                        ->set('email',  $user->email) // Configures a new claim, called "uid"
                         ->sign($signer, $secretKey) // creates a signature using  as key
                        ->getToken(); // Retrieves the generated token

		
		return $token;
	}
	
	public function verifyToken($jwt){
		$token =  (new Parser())->parse((string) $jwt);
		$userId = $token->getClaim('userId');
		$verified = false;

		if($userId){
			$user = resolve('App\Repositories\UsersRepository')->getById($userId);
			$signer = new Sha256();
			$verified = $token->verify($signer, $user->secret_key);
		}
		
		return $verified;
	}
	
	public function getUserFromToken($jwt){
		$token =  (new Parser())->parse((string) $jwt);
		$userId = $token->getClaim('userId');
		if($userId){
			$user = resolve('App\Repositories\UsersRepository')->getById($userId);
			return $user;
		}
	}
}