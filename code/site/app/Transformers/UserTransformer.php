<?php
namespace App\Transformers;

use App\Models\User;
use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract
{
	public function transform(User $user)
	{
	    return [
	        'id'      => (int) $user->id,
	        'name'   => $user->name,
	        'email'    => $user->email,
	        'is_active'    => $user->is_active,
	        'is_closed'    => $user->is_closed,
	        'close_reason'    => $user->close_reason,
	    ];
	}
}