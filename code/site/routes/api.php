<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users', 'Api\UsersController@get');
Route::post('/users', 'Api\UsersController@create');
Route::put('/users/{id}', 'Api\UsersController@update');
Route::delete('/users/{id}', 'Api\UsersController@delete');
