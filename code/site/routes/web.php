<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'Site'], function() {
    Route::match(['get', 'post'], '/login', 'AuthController@login')->name('site-login');
    Route::get('/logout', 'AuthController@logout')->name('site-logout');
    
    Route::get('/register/success', 'RegisterController@registerSucceess')->name('site-register-success');
    Route::get('/register/activation-success', 'RegisterController@resendActivationSucceess')->name('site-send-activation-success');
    Route::match(['get', 'post'],'/register/activate/resend', 'RegisterController@resendActivationMail')->name('site-resend-activation-mail');
    Route::get('/register/activate', 'RegisterController@activate')->name('site-activate-account');
    Route::match(['get', 'post'],'/register', 'RegisterController@register')->name('site-register');
    
    
    Route::match(['get', 'post'],'/account/edit', 'AccountController@edit')->name('site-edit-account');
    Route::match(['get', 'post'],'/account/close', 'AccountController@close')->name('site-close-account');
});

Route::get('/', 'Site\HomeController@index')->name('site-home');