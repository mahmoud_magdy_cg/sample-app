const webpack = require('webpack');
var path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractCSS = new ExtractTextPlugin('styles/[name].[hash].css');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

let t = require('webpack-mapping-plugin');
const MappingPlugin  = t.default;

module.exports = {
    entry:{
        main: './resources/assets/js/app.js',
        vendor: './resources/assets/js/vendor.js'
    },
    output:{
        filename: '[name].[hash].js',
        path: path.resolve(__dirname, 'public/assets'),
    },
    plugins:[
      extractCSS
       , new webpack.optimize.CommonsChunkPlugin({
                name: ['main', 'vendor', 'bootstrap'] // Specify the common bundle's name.
            }),
     new MappingPlugin({
        // ...
        fileName: 'assets-map.json',
        basePath: 'assets/'
      }),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      }),
       new webpack.optimize.UglifyJsPlugin(),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: {removeAll: true } },
            canPrint: true
        }),
    ],
    module:{
        rules:[
            {
                test: /\.(css|scss)$/,
                use: extractCSS.extract({ fallback: 'style-loader',
                                        use: [
                                                {
                                                    loader:'css-loader'
                                                },
                                                'sass-loader',
                                            ]
                                        }),
            },{
                test: /\.(png|jpe?g|gif)$/,
                use: 'url-loader?limit=1000&name=images/[name].[hash:12].[ext]'
            },{
                test: /\.(eot|woff2?|ttf)$/,
                use: 'url-loader?name=assets/[name].[hash:12].[ext]'
            },{
                test: /\.(svg)$/,
                use: 'svg-url-loader?limit=1024'
            }
        ]
    },
   // watch: true,
    watchOptions: {
       /* aggregateTimeout: 300,
        poll: 1000,*/
        ignored: /node_modules/
    },
    //devtool: "source-map"
};