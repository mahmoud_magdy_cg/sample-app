import { Component } from "@angular/core";
import { User } from "../../models/user.model";
import { UsersService } from "../../services/users.service";

import { ListResult } from '../../services/api/list-result.interface'
import { Observable } from 'rxjs/Observable'
import { Subscription } from 'rxjs/Subscription'

@Component({
    moduleId: module.id+"",
    selector: 'users-list',
    templateUrl: './users-list.component.html'
})

export class UsersListComponent {
    title:string = 'Users List';

    total:number;
    itemsPerPage:number;
    users:User[];

    pageNumber: number = 1;
    usersSubscribtion: Subscription;
    isLoading:boolean = false;

    constructor(private usersService:UsersService){
    }

    ngOnInit() :void{
        this.getUsers();    
    }

    getUsers() {
        if(this.usersSubscribtion) {
            this.usersSubscribtion.unsubscribe();
        }

        this.isLoading = true;
        this.usersSubscribtion = 
            this.usersService.getUsers(this.pageNumber).subscribe(
                (list:ListResult<User>) => {
                    this.users = list.data;
                    this.total = list.meta.pagination.total;
                    this.itemsPerPage = list.meta.pagination.per_page;
                    this.isLoading = false;
                }
            );
        
    }
    onPageChange(pageNumber :number) {
        this.pageNumber = pageNumber;
        this.getUsers();
    }
}