import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

require('./app.component.scss');
@Component({
  moduleId: module.id+"",
  selector: 'my-app',
  templateUrl: './app.component.html'
})
export class AppComponent{
  title = 'Sample App';
}