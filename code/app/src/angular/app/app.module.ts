import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { HttpModule }    from '@angular/http';

//components
import { AppComponent }  from './app.component';
import { UsersListComponent }  from './users-list/users-list.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from "./app-routing.module";

import { UsersService } from "../services/users.service";

import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  imports:      [ 
    BrowserModule,
    FormsModule  ,
    HttpModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    NgxPaginationModule
   ],
  declarations: [ 
    AppComponent,
    UsersListComponent
  ],
  providers: [
    UsersService
   ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
