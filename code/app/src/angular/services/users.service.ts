import { Injectable } from '@angular/core';

import { Headers, Http, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { User } from '../models/user.model';

import { config } from "../config";

import { ListResult } from './api/list-result.interface'
import { Observable } from 'rxjs/Observable'

@Injectable()
export class UsersService {
    private usersURL = config.APIService+'/users';
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

     getUsers(page: number = 1): Observable<ListResult<User>>  {
        let params: URLSearchParams = new URLSearchParams();
        params.set('token', config.APIToken);
        if (page) params.set('page', String(page));

        let requestOptions = new RequestOptions();
        requestOptions.search = params;
        return this.http.get(this.usersURL, requestOptions)
            .map(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}