import './angular/main.ts';

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept();
}