var webpack = require('webpack');
var path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var helpers = require('./helpers');

const extractCSS = new ExtractTextPlugin('styles/[name].css');

module.exports = {
  entry: {
    main: './src/index.js',
    vendor: './src/vendor.ts',
    config: './src/angular/config.ts',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins:[
    // Workaround for angular/angular#11580
    new webpack.ContextReplacementPlugin(
      // The (\\|\/) piece accounts for path separators in *nix and Windows
      /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
      helpers.root('./src'), // location of your src
      {} // a map of your routes
    ),
    new HtmlWebpackPlugin({template:'./src/index.html',
                            hash:true})
    ,extractCSS
    , new webpack.optimize.CommonsChunkPlugin({
                name: ['commons', 'vendor', 'config', 'bootstrap'] // Specify the common bundle's name.
            })
   // , new webpack.optimize.UglifyJsPlugin()
    ,new webpack.NamedModulesPlugin()
  ],
  module: {
    rules:[
     /* {
        test: /\.component\.(css|scss)$/,
        use: ['to-string-loader', 'css-loader', 'sass-loader']
                                
      }
      ,*//*{
        test: /\.(css|scss)$/,
        use: extractCSS.extract({ 
          fallback: 'style-loader',
          use: [ 
            { 
              loader: 'css-loader', 
              options: {
                sourceMap: true
              }
            },
            'sass-loader'
            ]
          }),
       // exclude: /\.component\.(css|scss)$/
      },*/{
        test: /\.(css|scss)$/,
        use: [
          'style-loader',
          {
            loader:'css-loader',
            options: {
              sourceMap: true
            }
          },
          'sass-loader'
        ]
      },{
        test: /\.(less)$/,
        use: extractCSS.extract({ 
          fallback: 'style-loader',
          use: ['css-loader', 'less-loader']
        }),
        exclude: /\.component\.less$/        
      },{
        test: /\.(png|jpe?g|gif)$/,
        use: 'url-loader?limit=1000&name=images/[name].[hash:12].[ext]'
      },{
        test: /\.(eot|woff2?|ttf)$/,
        use: 'url-loader?name=assets/[name].[hash:12].[ext]'
      },{
        test: /\.(svg)$/,
        use: 'svg-url-loader?limit=1024'
      },{
        test: /\.html$/,
        use: 'html-loader',
      },{
        test: /\.tsx?$/,
        use: ['awesome-typescript-loader', 'angular2-template-loader']
      }
    ]
  },
  resolve: {
	    // Add `.ts` and `.tsx` as a resolvable extension.
	    extensions: [".webpack.js", ".web.js", ".js", ".ts",'.tsx', '.jsx'],
      alias:{
        '@ng-bootstrap/ng-bootstrap': '@ng-bootstrap/ng-bootstrap/bundles/ng-bootstrap.js'
      }
	  },
  devServer:{
    contentBase: path.join(__dirname, 'dist'),
    historyApiFallback: true
  },
  devtool: "source-map"

}
